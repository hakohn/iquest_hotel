﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace iquest_hotel
{
    public sealed class HotelBuilder
    {
        private Hotel hotel;
        private BookingsGrouper bookingsGrouper;
        private HashSet<Customer> customers;
        private Interval<DateTime> Interval;

        public Customer GetCustomerByID(int id)
        {
            return customers.First((cust) => cust.ID == id);
        }

        #region Week1
        /// <summary> Randomly build a hotel. </summary>
        /// <param name="roomCount"> The total room count. </param>
        /// <param name="reservationCount"> The total reservation count. </param>
        /// <param name="interval"> The dates between which the random values will be generated. </param>
        public void BuildRandomly(int roomCount, int reservationCount, Interval<DateTime> interval)
        {
            Interval = interval;

            // The variables / methods used for random generation
            var roomOccupantsRange = new Interval<int>(2, 6);
            var maximumDayCountReservation = 6;
            var random = new Random();
            var existingIDs = new HashSet<int>();
            var existingCustomerIDs = new HashSet<int>();
            int RandomID() {
                int id = 0;
                do { id = random.Next(10000); } while (existingIDs.Contains(id));
                existingIDs.Add(id);
                return id;
            }
            int RandomExistingID(HashSet<int> blackListedIDs)
            {
                int id = 0;
                do { id = random.Next(10000); } while (!blackListedIDs.Contains(id) && blackListedIDs.Count != 0);
                return id;
            }


            // Building the hotel rooms
            hotel = new Hotel();
            for(int number = 0; number < roomCount; number++)
            {
                int id = RandomID();
                int capacity = random.Next(roomOccupantsRange.Begin, roomOccupantsRange.End + 1);

                hotel.AddRoom(new Room(id, number, capacity));
            }


            // Creating the reservation list and generating random user IDs (which will be used in the next section for creating the users)
            bookingsGrouper = new BookingsGrouper();
            bookingsGrouper.AddRoomIDsRange(from room in hotel.Rooms orderby room.Value.Number select room.Value.ID);
            for(int i = 0; i < reservationCount; i++)
            {
                int id = RandomID();
                int roomID = hotel.GetRoomByNumber(random.Next(hotel.Rooms.Count)).ID;
                var begins = interval.Begin.AddDays(random.Next((interval.End - interval.Begin).Days));
                var ends = begins.AddDays(random.Next(maximumDayCountReservation));
                if(ends > interval.End)
                {
                    ends = interval.End;
                }

                bookingsGrouper.AddReservation(new Reservation(id, roomID, new Interval<DateTime>(begins, ends)));

                int occupantCount = random.Next(1, roomOccupantsRange.End + 1);
                for(int j = 0; j < occupantCount; j++)
                {
                    int custId;
                    // There's a 50/50 chance for the customerID to be of a new customer, or an already existing one
                    if (random.Next(100) < 70)
                    {
                        // New customer
                        custId = RandomID();
                        existingCustomerIDs.Add(custId);
                    }
                    else
                    {
                        // Existing customer
                        custId = RandomExistingID(existingCustomerIDs);
                    }
                    bookingsGrouper.Reservations[id].AddCustomerID(custId);
                }
            }


            // Creating the customer list, based on the existingCustomerIDs list
            customers = new HashSet<Customer>();
            foreach(var custID in existingCustomerIDs)
            {
                var reservations = bookingsGrouper.GetReservationsByCustomerID(custID);
                var reservationIDs = (from res in reservations select res.ID).ToHashSet();
                var country = Names.RandomCountry;
                Func<int, DateTime> AccomodationDateMethod = bookingsGrouper.GetFirstAccomodationDate;

                // Check if the reservations this user has have all their occupant count of 1. If so, then instantiate the customer as a company
                if ((from res in reservations select res.CustomerIDs.Count).Max() == 1)
                {
                    var name = Names.RandomCompanyName;
                    var companySuffix = Names.RandomCompanySuffix;
                    customers.Add(new Company(custID, country, name, companySuffix, AccomodationDateMethod, reservationIDs));
                }
                else
                {
                    var name = Names.RandomName;
                    var surname = Names.RandomSurname;
                    customers.Add(new Person(custID, country, name, surname, AccomodationDateMethod, reservationIDs));
                }

            }

            // Printing our randomly-generated lists into import text files, so that we do not have to hand-write them for our further actions.
            // Customers
            using (StreamWriter writer = File.CreateText("customers.txt")) 
            {
                // Order alphabetically by name
                foreach (var elem in from cust in customers orderby cust.FullName select cust)
                {
                    writer.WriteLine(elem.ToImportString());
                }
            }
            // Reservations
            using (StreamWriter writer = File.CreateText("reservations.txt"))
            {
                // Order by room number
                foreach (var elem in from res in bookingsGrouper.Reservations orderby res.Value.RoomID select res)
                {
                    writer.WriteLine(elem.Value.ToImportString());
                }
            }            
            // Rooms
            using (StreamWriter writer = File.CreateText("rooms.txt"))
            {

                foreach (var elem in from room in hotel.Rooms orderby room.Value.Number select room)
                {
                    writer.WriteLine(elem.Value.ToImportString());
                }
            }
        }
        #endregion

        #region Week2
        /// <summary> Loads the customers, reservations and rooms from a file. </summary>
        public void LoadFromFile(string customerPath, string reservationPath, string roomPath)
        {
            hotel = new Hotel(DataIO.ImportAdapter.Import(roomPath));

            bookingsGrouper = new BookingsGrouper(DataIO.ImportAdapter.Import(reservationPath));
            bookingsGrouper.AddRoomIDsRange(from room in hotel.Rooms orderby room.Value.Number select room.Value.ID);

            // Calculate the bookings' time interval
            Interval = new Interval<DateTime>(
                (from res in bookingsGrouper.Reservations.Values select res.Interval.Begin).Min(),
                (from res in bookingsGrouper.Reservations.Values select res.Interval.End).Max()
                );

            customers = new HashSet<Customer>();
            foreach (var dict in DataIO.ImportAdapter.Import(customerPath))
            {
                switch(dict["Type"])
                {
                    case nameof(Company):
                        customers.Add(new Company(dict, bookingsGrouper.GetFirstAccomodationDate));
                        break;
                    case nameof(Person):
                        customers.Add(new Person(dict, bookingsGrouper.GetFirstAccomodationDate));
                        break;
                }
            }
        }

        /// <summary> Export to a file details about all the customers who have a given keyword found within their names. </summary>
        public void ExportByKeyword(string keyword, string destinationPath)
        {
            var chosenOnes = new List<Customer>();

            foreach(var cust in customers)
            {
                if (cust.FullName.Contains(keyword))
                {
                    chosenOnes.Add(cust);
                }
            }

            DataIO.ExportAdapter.Export(chosenOnes, destinationPath);
        }

        /// <summary> Export to a file details about the first given amount of customers from each different country.
        /// They'll be sorted by their first accomodation date. </summary>
        public void ExportByCountry(int count, string destinationPath)
        {
            var customerDictionary = new Dictionary<string, List<Customer>>();

            // Making a list of customers for each different country
            foreach(var cust in customers)
            {
                if(customerDictionary.ContainsKey(cust.Country) == false)
                {
                    customerDictionary.Add(cust.Country, new List<Customer>());
                }

                customerDictionary[cust.Country].Add(cust);
            }

            // Sorting time
            var chosenOnes = new List<Customer>();
            foreach(var elem in customerDictionary)
            {
                elem.Value.Sort((x, y) => x.FirstAccomodationDate.CompareTo(y.FirstAccomodationDate));
                elem.Value.RemoveRange(count, elem.Value.Count - count);

                //Wrapping them all up into a single list
                foreach(var cust in elem.Value)
                {
                    chosenOnes.Add(cust);
                }
            }


            DataIO.ExportAdapter.Export(chosenOnes, destinationPath);
        }

        /// <summary> Used only for demonstration purposes. Will probably be removed once we actually put the 
        /// bookingsGrouper.Display method to good use. </summary>
        public void DisplayReservations()
        {
            bookingsGrouper.DisplayBookingsGroupedByRooms();
        }
        #endregion

        #region Week3
        public void FixOverlaps()
        {
            bookingsGrouper.ResolveOverlappingReservations();
            bookingsGrouper.SortByDate();

            //Exporting important reservations
            using (StreamWriter writer = File.CreateText("ImportantReservations.csv"))
            {
                // Title
                writer.Write(string.Join(", ", "Booking ID", "Room No."));
                for (var date = Interval.Begin; date <= Interval.End; date = date.AddDays(1))
                {
                    writer.Write($", {date.ToShortDateString()}");
                }
                writer.WriteLine();

                
                var prevRoomID = -1;
                // Contents
                foreach (var elem in from res in bookingsGrouper.Reservations.Values orderby hotel.Rooms[res.RoomID].Number select res)
                {
                    if(prevRoomID != elem.RoomID && prevRoomID != -1)
                    {
                        writer.WriteLine();
                        writer.WriteLine();
                    }
                    prevRoomID = elem.RoomID;

                    writer.Write(string.Join(", ", elem.ID, hotel.Rooms[elem.RoomID].Number));
                    for(var date = Interval.Begin; date <= Interval.End; date = date.AddDays(1))
                    {
                        writer.Write(", ");
                        char c = ' ';
                        if (elem.Interval.Contains(date))
                        {
                            c = '=';
                        }
                        writer.Write(new string(c, 8));
                    }

                    writer.WriteLine();
                }
            }
        }
        #endregion
    }

}
