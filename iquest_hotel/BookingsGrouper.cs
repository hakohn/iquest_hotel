﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iquest_hotel
{
    public class BookingsGrouper
    {
        public Dictionary<int, Reservation> Reservations { get; protected set; }
        public HashSet<int> RoomIDs { get; protected set; }

        /// <summary> Create an empty reservation list. </summary>
        public BookingsGrouper()
        {
            Reservations = new Dictionary<int, Reservation>();
            RoomIDs = new HashSet<int>();
        }
        /// <summary> Load the reservation list from a list of dictionaries. </summary>
        public BookingsGrouper(List<Dictionary<string, string>> dictList)
        {
            Reservations = new Dictionary<int, Reservation>();
            RoomIDs = new HashSet<int>();
            foreach (var dict in dictList)
            {
                var res = new Reservation(dict);
                Reservations.Add(res.ID, res);
            }
        }


        public void AddReservation(Reservation reservation)
        {
            Reservations.Add(reservation.ID, reservation);
        }

        public void AddRoomIDsRange(IEnumerable<int> roomIDs)
        {
            foreach(var id in roomIDs)
            {
                RoomIDs.Add(id);
            }
        }

        public DateTime GetFirstAccomodationDate(int customerID)
        {
            return (from res in Reservations.Values where res.CustomerIDs.Contains(customerID) select res.Interval.Begin).Min();
        }

        /// <summary>
        /// Get all the reservations which have the customerID provided as one of the customers.
        /// </summary>
        public HashSet<Reservation> GetReservationsByCustomerID(int customerID)
        {
            return (from res in Reservations.Values where res.CustomerIDs.Contains(customerID) select res).ToHashSet();
        }

        /// <summary> 
        /// Group the reservations by the rooms they are assigned to. A dictionary will be returned,
        /// which will have the key set to the roomID, and the value set to a list of reservations. 
        /// </summary>
        public Dictionary<int, HashSet<Reservation>> GroupByRooms()
        {
            var groupDictionary = new Dictionary<int, HashSet<Reservation>>();

            foreach (var res in Reservations)
            {
                if (groupDictionary.ContainsKey(res.Value.RoomID) == false)
                {
                    groupDictionary.Add(res.Value.RoomID, new HashSet<Reservation>());
                }

                groupDictionary[res.Value.RoomID].Add(res.Value);
            }

            return groupDictionary;
        }

        /// <summary> Sort the whole reservations dictionary by reservation dates. </summary>
        public void SortByDate()
        {
            var resList = Reservations.ToList();
            resList.Sort((x, y) => x.Value.Interval.CompareTo(y.Value.Interval));

            Reservations = new Dictionary<int, Reservation>();
            foreach(var e in resList)
            {
                Reservations.Add(e.Key, e.Value);
            }
        }

        /// <summary> Check if the given room is occupied on the given interval. </summary>
        public bool IsRoomOccupiedOn(int roomID, Interval<DateTime> interval)
        {
            var resDict = GroupByRooms();
            if(!resDict.ContainsKey(roomID))
            {
                return false;
            }

            var resList = resDict[roomID];
            foreach (var res in resList)
            {
                if(res.Interval.Intersects(interval))
                {
                    return true;
                }
            }

            return false;
        }
        public bool IsRoomOccupiedOn(int roomID, DateTime date)
        {
            return IsRoomOccupiedOn(roomID, new Interval<DateTime>(date, date));
        }

        /// <summary> Get a list of all the reservations which have overlapping dates with each other for the given room. </summary>
        public HashSet<Reservation> OverlappingReservationsByRoom(int roomID)
        {
            var overlappingReservations = new HashSet<Reservation>();
            var groups = GroupByRooms();
            if (groups.ContainsKey(roomID))
            {
                var resList = groups[roomID];
                for (int i = 0; i < resList.Count; i++)
                {
                    for (int j = i + 1; j < resList.Count; j++)
                    {
                        if (resList.ElementAt(i).Interval.Intersects(resList.ElementAt(j).Interval))
                        {
                            overlappingReservations.Add(resList.ElementAt(i));
                            overlappingReservations.Add(resList.ElementAt(j));
                        }
                    }
                }
            }

            return overlappingReservations;
        }

        /// <summary> Attempt resolving the overlapping dates of the rooms without chaning the reservation dates. </summary>
        public void ResolveOverlappingReservations()
        {
            foreach(var roomID in RoomIDs)
            {
                foreach(var res in OverlappingReservationsByRoom(roomID))
                {
                    foreach(var tarRoomID in RoomIDs)
                    {
                        if(tarRoomID != roomID && !IsRoomOccupiedOn(tarRoomID, res.Interval))
                        {
                            Reservations[res.ID].SetRoomID(tarRoomID);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// A method to display the contents of the GroupByRooms method into the console in an easy to read format.
        /// </summary>
        public void DisplayBookingsGroupedByRooms()
        {
            var groups = GroupByRooms();
            
            foreach (var key in groups.Keys)
            {
                foreach (var elem in groups[key])
                {
                    Console.WriteLine(elem.ToExportString());
                }
                Console.WriteLine();
            }
        }
    }
}
