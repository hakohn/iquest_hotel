﻿namespace iquest_hotel
{
    public interface IImportable
    {
        string ToImportString();
    }
}
