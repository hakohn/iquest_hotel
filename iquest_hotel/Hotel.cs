﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iquest_hotel
{
    public sealed class Hotel
    {
        /// <summary> A dictionary of rooms. The rooms are to be accessed by their ID. </summary>
        public Dictionary<int, Room> Rooms { get; private set; }

        public Hotel()
        {
            Rooms = new Dictionary<int, Room>();
        }
        /// <summary> Load the hotel list from a list of dictionaries. </summary>
        public Hotel(List<Dictionary<string, string>> dictList)
        {
            Rooms = new Dictionary<int, Room>();
            foreach(var dict in dictList)
            {
                var room = new Room(dict);
                Rooms.Add(room.ID, room);
            }
        }

        public void AddRoom(Room room)
        {
            Rooms.Add(room.ID, room);
        }

        public Room GetRoomByNumber(int roomNumber)
        {
            return Rooms.First((x) => x.Value.Number == roomNumber).Value;
        }
    }
}
