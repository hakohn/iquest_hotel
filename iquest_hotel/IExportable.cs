﻿namespace iquest_hotel
{
    public interface IExportable
    {
        string ToExportString();
    }
}
