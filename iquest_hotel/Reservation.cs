﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace iquest_hotel
{
    public sealed class Reservation : IImportable, IExportable
    {
        public int ID { get; }
        public int RoomID { get; private set; }
        public Interval<DateTime> Interval { get; }
        public HashSet<int> CustomerIDs { get; private set; }

        /// <summary> Load each stat by direct parameters. </summary>
        public Reservation(int id, int roomID, Interval<DateTime> interval, HashSet<int> customerIDs = null)
        {
            ID = id;
            RoomID = roomID;
            Interval = interval;
            CustomerIDs = customerIDs ?? new HashSet<int>();
        }
        /// <summary> Load the reservation stats from a dictionary. </summary>
        public Reservation(Dictionary<string, string> dict)
        {
            ID = Convert.ToInt32(dict["ID"]);
            RoomID = Convert.ToInt32(dict["RoomID"]);
            Interval = new Interval<DateTime>(
                DateTime.ParseExact(dict["Starts"], "dd MMM yy", CultureInfo.InvariantCulture),
                DateTime.ParseExact(dict["Ends"], "dd MMM yy", CultureInfo.InvariantCulture)
                );
            CustomerIDs = (from custId in dict["Customers"].Split(',') select Convert.ToInt32(custId)).ToHashSet();
        }


        public void AddCustomerID(int id)
        {
            CustomerIDs.Add(id);
        }

        public void SetRoomID(int id)
        {
            RoomID = id;
        }

        public string ToImportString()
        {
            return string.Join(" | ",
                $"ID: {ID}",
                $"RoomID: {RoomID}",
                $"Starts: {Interval.Begin.ToShortDateString()}",
                $"Ends: {Interval.End.ToShortDateString()}",
                $"Customers: {string.Join(',', from custId in CustomerIDs orderby custId select custId)}"
            );
        }

        public string ToExportString()
        {
            return string.Join("\t\t",
                $"ID: {string.Format("{0, -7}", ID)}",
                $"RoomID: {string.Format("{0, -6}", RoomID)}",
                $"Interval: {Interval.ToString()}"
            );
        }
    }
}
