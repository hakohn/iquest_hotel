﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iquest_hotel
{
    public abstract class Customer : IImportable, IExportable
    {
        public int ID { get; }
        public virtual string FullName { get; protected set; }
        public string Country { get; }
        public HashSet<int> ReservationIDs { get; protected set; }
        public DateTime FirstAccomodationDate { get => FirstAccomodationDateMethod(ID); }
        protected Func<int, DateTime> FirstAccomodationDateMethod { get; }

        /// <summary> Load each stat by direct parameters. </summary>
        public Customer(int id, string country, Func<int, DateTime> firstAccomodationDateMethod, HashSet<int> reservationIDs = null)
        {
            ID = id;
            Country = country;
            FirstAccomodationDateMethod = firstAccomodationDateMethod;
            ReservationIDs = reservationIDs ?? new HashSet<int>();
        }
        /// <summary> Load the customer stats from a dictionary. </summary>
        public Customer(Dictionary<string, string> dict, Func<int, DateTime> firstAccomodationDateMethod)
        {
            ID = Convert.ToInt32(dict["ID"]);
            Country = dict["Country"];
            ReservationIDs = (from resId in dict["Reservations"].Split(',') select Convert.ToInt32(resId)).ToHashSet();
            FirstAccomodationDateMethod = firstAccomodationDateMethod;
        }


        public string ToImportString()
        {
            return string.Join(" | ", 
                $"ID: {ID}",
                $"Type: {GetType().Name}",
                $"Name: {FullName}",
                $"Country: {Country}",
                $"Reservations: {string.Join(',', from resID in ReservationIDs orderby resID select resID)}"
            );
        }

        public string ToExportString()
        {
            return string.Join("\t\t",
                $"Name: {string.Format("{0, -30}", FullName)}",
                $"Country: {string.Format("{0, -20}", Country)}",
                $"First accomodation date: {FirstAccomodationDate.ToShortDateString()}"
            );
        }
    }
}
