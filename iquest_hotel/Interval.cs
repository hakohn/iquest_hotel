﻿using System;
using System.Collections.Generic;

namespace iquest_hotel
{
    public class Interval<T> : IComparable<Interval<T>>, IEquatable<Interval<T>>
        where T : IComparable<T>, IEquatable<T>
    {
        public T Begin { get; set; }
        public T End { get; set; }


        public Interval()
        {
        }
        public Interval(T begin, T end)
        {
            Begin = begin;
            End = end;
        }


        public bool Equals(Interval<T> other)
        {
            if(Begin.Equals(other.Begin))
            {
                if(End.Equals(other.End))
                {
                    return true;
                }
            }

            return false;
        }

        public int CompareTo(Interval<T> other)
        {
            if(Begin.Equals(other.Begin))
            {
                return End.CompareTo(other.End);
            }

            return Begin.CompareTo(other.Begin);
        }

        /// <summary> Check if the interval contains a certain value. </summary>
        public bool Contains(T obj)
        {
            if(Begin.CompareTo(obj) <= 0 && 0 <= End.CompareTo(obj))
            {
                return true;
            }

            return false;
        }

        /// <summary> Check if two intervals intersect each other. </summary>
        public bool Intersects(Interval<T> other)
        {
            if(Contains(other.Begin) || Contains(other.End) || other.Contains(Begin) || other.Contains(End))
            {
                return true;
            }

            return false;
        }
        
        /// <summary> Get the intersection interval of two intervals. If there is no intersection, null is returned. </summary>
        public Interval<T> Intersection(Interval<T> other)
        {
            return !other.Intersects(this) ? null : new Interval<T>
            {
                Begin = Begin.CompareTo(other.Begin) < 0 ? other.Begin : Begin,
                End = End.CompareTo(other.End) < 0 ? End : other.End
            };
        }

        public override string ToString()
        {
            string start = string.Empty;
            string finish = string.Empty;

            if(typeof(T) == typeof(DateTime))
            {
                start = ((DateTime)(object)Begin).ToShortDateString();
                finish = ((DateTime)(object)End).ToShortDateString();
            }
            else
            {
                start = Begin.ToString();
                finish = End.ToString();
            }

            return $"{start} - {finish}";
        }
    }
}
