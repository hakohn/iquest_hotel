﻿using System;

namespace iquest_hotel
{
    class Program
    {
        /// <summary> Randomly generate the hotel and all its reservations / customers. </summary>
        static void RandomlyGenerateHotel()
        {
            var hotelBuilder = new HotelBuilder();
            hotelBuilder.BuildRandomly(40, 200, new Interval<DateTime>(DateTime.Today, DateTime.Today.AddMonths(1)));
        }

        /// <summary> Import all the customers from a file and then export them to different files, based on different criteria. </summary>
        static void ImportAndGroup()
        {
            var hotelBuilder = new HotelBuilder();
            hotelBuilder.LoadFromFile(@"customers.txt", @"reservations.txt", @"rooms.txt");

            hotelBuilder.ExportByKeyword("Daniel", @"CustomersByKeyword.txt");
            hotelBuilder.ExportByCountry(10, @"FirstCustomersByCountry.txt");
            hotelBuilder.FixOverlaps();
            hotelBuilder.DisplayReservations();

            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            //RandomlyGenerateHotel();
            ImportAndGroup();
        }
    }
}
