﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace iquest_hotel
{
    public class DataImportAdapter
    {
        /// <summary> Import the elements from a file. </summary>
        public List<Dictionary<string, string>> Import(string path)
        {
            var dictList = new List<Dictionary<string, string>>();

            using (var reader = File.OpenText(path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    dictList.Add(new Dictionary<string, string>());

                    foreach(var pair in line.Split(" | ", StringSplitOptions.RemoveEmptyEntries))
                    {
                        var words = pair.Split(": ", StringSplitOptions.RemoveEmptyEntries);
                        dictList.Last().Add(words[0], words[1]);
                    }
                }
            }

            return dictList;
        }
    }
}
