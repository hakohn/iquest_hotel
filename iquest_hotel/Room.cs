﻿using System;
using System.Collections.Generic;

namespace iquest_hotel
{
    public sealed class Room : IImportable, IExportable
    {
        public int ID { get; }
        public int Number { get; }
        public int Capacity { get; }


        /// <summary> Load each stat by individual parameters. </summary>
        public Room(int id, int number, int capacity)
        {
            ID = id;
            Number = number;
            Capacity = capacity;
        }
        /// <summary> Load the room stats from a dictionary. </summary>
        public Room(Dictionary<string, string> dict)
        {
            ID = Convert.ToInt32(dict["ID"]);
            Number = Convert.ToInt32(dict["Number"]);
            Capacity = Convert.ToInt32(dict["Capacity"]);
        }


        public string ToImportString()
        {
            return string.Join(" | ",
                $"ID: {ID}",
                $"Number: {Number}",
                $"Capacity: {Capacity}"
            );
        }

        public string ToExportString()
        {
            return string.Join("\t\t",
                $"ID: {string.Format("{0, -6}", ID)}",
                $"Number: {string.Format("{0, -4}", Number)}",
                $"Capacity: {string.Format("{0, -2}", Capacity)}"
            );
        }
    }
}
