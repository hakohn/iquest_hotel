﻿using System;

namespace iquest_hotel
{
    /// <summary>
    /// This class shouldn't even exist. I am using it for and only for the purpose of randomly generating the customer list.
    /// Should we no longer be in need of a randomly generated list and have our customers registered from a hand-written file,
    /// this will be removed.
    /// </summary>
    public static class Names
    {
        private static readonly Random random = new Random();

        private static readonly string[] names = {
            "Adrian", "Adriana", "Alexandru", "Alexandra", "Alin", "Alina", "Ana", "Andreea", "Andrei",
            "Beatrice", "Bogdan", "Catalin", "Daniel", "Daniela", "Dragos", "Emanuel", "Emanuela",
            "Evelyn", "Florin", "Florina", "Florentina", "Gabriel", "Gabriela", "Ioana", "Ion", "Ionut",
            "Iulia", "Mihaela", "Mihai", "Rares", "Radu", "Razvan", "Rebeca", "Robert", "Roberta", "Serban",
            "Stefan", "Teodor", "Teodora", "Vlad"
        };
        private static readonly string[] surnames = {
            "Baciu", "Busuiocescu", "Cerbulescu", "Curcan", "Dinca", "Dogaru", "Dorobantu", "Dumitrescu",
            "Enache", "Gainusa", "Ghita", "Lapadat", "Lupan", "Manolache", "Manolescu", "Marin", "Negrea",
            "Neguletu", "Neremzoiu", "Paun", "Petrof", "Pirvu", "Putin", "Popescu", "Radita", "Stanescu",
            "Stefan", "Smarandache", "Tanase", "Taran", "Vasilescu", "Visan", "Vrabii"
        };
        private static readonly string[] companies = {
            "Blizzard", "CD Projekt Red", "Valve", "Microsoft", "Philips", "Samsung", "ASUS", "HatTrick", "Sporeglow",
            "Sony", "Gigabyte", "Radovan", "MSI", "Bungie", "343", "Foreverdark Woods", "Bathory", "Ensiferum",
            "Grinding Gear Games", "Hammerfall", "Wintersun", "Abney Park", "H.M.J.", "Larian"
        };
        private static readonly string[] companySuffixes = {
            "S.R.L.", "Entertainment", "Studios", "Channel", "Industries"
        };
        private static readonly string[] countries = {
            "Romania", "United Kingdom", "Bulgaria", "Italy", "Germany", "Spain", "Poland", "USA", "Norway", "Finland",
            "Albania", "Russia", "China", "Japan", "Canada", "Austria", "Slovenia"
        };


        public static string RandomName => names[random.Next(names.Length)];
        public static string RandomSurname => surnames[random.Next(surnames.Length)];
        public static string RandomCompanyName => companies[random.Next(companies.Length)];
        public static string RandomCompanySuffix => companySuffixes[random.Next(companySuffixes.Length)];
        public static string RandomCountry => countries[random.Next(countries.Length)];
    }
}
