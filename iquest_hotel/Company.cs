﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iquest_hotel
{
    public sealed class Company : Customer
    {
        public override string FullName => $"{Name} {NameSuffix}";
        public string Name { get; }
        public string NameSuffix { get; }

        /// <summary> Load each stat by direct parameters. </summary>
        public Company(int id, string country, string name, string nameSuffix, Func<int, DateTime> firstAccomodationDateMethod, HashSet<int> reservationIDs = null) :
            base(id, country, firstAccomodationDateMethod, reservationIDs)
        {
            Name = name;
            NameSuffix = nameSuffix;
        }
        /// <summary> Load the customer stats from a dictionary. </summary>
        public Company(Dictionary<string, string> dict, Func<int, DateTime> firstAccomodationDateMethod) :
            base(dict, firstAccomodationDateMethod)
        {
            // Each customer can have multiple names, but only one surname / company suffix
            var words = dict["Name"].Split(' ');
            Name = string.Join(' ', words.SkipLast(1));
            NameSuffix = words.Last();
        }
    }
}
