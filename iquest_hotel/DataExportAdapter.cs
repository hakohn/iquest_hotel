﻿using System.Collections.Generic;
using System.IO;

namespace iquest_hotel
{
    public class DataExportAdapter
    {
        /// <summary> Export the elements of a list, as a string, to a file. </summary>
        public void Export<T>(List<T> elements, string destinationPath) where T : IExportable
        {
            using (var writer = File.CreateText(destinationPath))
            {
                foreach (var elem in elements)
                {
                    writer.WriteLine(elem.ToExportString());
                }
            }
        }
    }
}
