﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iquest_hotel
{
    public sealed class Person : Customer
    {
        public override string FullName => $"{Name} {Surname}";
        public string Name { get; }
        public string Surname { get; }

        /// <summary> Load each stat by direct parameters. </summary>
        public Person(int id, string country, string name, string surname, Func<int, DateTime> firstAccomodationDateMethod, HashSet<int> reservationIDs = null) : 
            base(id, country, firstAccomodationDateMethod, reservationIDs)
        {
            Name = name;
            Surname = surname;
        }
        /// <summary> Load the customer stats from a dictionary. </summary>
        public Person(Dictionary<string, string> dict, Func<int, DateTime> firstAccomodationDateMethod) : 
            base(dict, firstAccomodationDateMethod)
        {
            // Each customer can have multiple names, but only one surname / company suffix
            var words = dict["Name"].Split(' ');
            Name = string.Join(' ', words.SkipLast(1));
            Surname = words.Last();
        }
    }
}
